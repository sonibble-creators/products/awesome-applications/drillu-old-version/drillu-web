/**
 * # ArticleItemCard
 *
 * card to show the item article
 * contain some information and summary data about the article
 *
 * @returns JSX.Element
 */
export default function ArticleItemCard(): JSX.Element {
  return (
    <div className="flex flex-col p-3 rounded-3xl bg-white bg-opacity-50 backdrop-blur-3xl border-2 border-white transition-all duration-500 hover:bg-white hover:-translate-y-3 hover:shadow-2xl hover:shadow-gray-200">
      <div className="flex w-full rounded-2xl overflow-hidden h-[200px]">
        <img
          src="https://images.unsplash.com/photo-1627483262268-9c2b5b2834b5?ixlib=rb-1.2.1&ixid=MnwxMjA3fDF8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80"
          alt=""
          className="h-full object-cover w-full"
        />
      </div>
      <div className="flex mt-5">
        <h3 className="font-heading font-semibold text-black text-xl">
          The unseen of spending three years at Pixelgrade
        </h3>
      </div>

      <div className="flex gap-2 mt-5">
        <span className="bg-pink-200 text-pink-500 px-3 py-1 rounded-xl text-sm font-medium transition-all duration-300 hover:scale-110">
          Design
        </span>
      </div>

      <div className="flex mt-3 gap-4 items-center">
        <div className="flex h-10 w-10 rounded-full overflow-hidden transition-all duration-300 hover:scale-110">
          <img
            src="https://images.unsplash.com/photo-1433888104365-77d8043c9615?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1746&q=80"
            alt=""
            className="h-full w-full object-cover"
          />
        </div>
        <div className="flex flex-col">
          <span className="text-gray-800 font-medium text-lg">Floyd Miles</span>
          <span className="text-gray-500">32 min ago</span>
        </div>
      </div>
    </div>
  );
}
