import { Icon } from "@iconify/react";

/**
 * # CreatorArticleItemCard
 *
 * the aticle item card for the creator page
 *
 * @returns JSX.Element
 */
export default function CreatorArticleItemCard(): JSX.Element {
  return (
    <div className="flex p-3 rounded-3xl border border-gray-200 gap-4 relative transition-all duration-300 hover:-translate-y-2 bg-white">
      <div className="flex overflow-hidden h-[85px] w-[85px] rounded-2xl group-hover:shadow-xl hover:shadow-gray-200 hover:-translate-y-2 transition-all duration-300">
        <img
          src="https://images.unsplash.com/photo-1452570053594-1b985d6ea890?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
          alt=""
          className="h-full w-full object-cover"
        />
      </div>

      <div className="flex flex-col gap-1">
        <h3 className="font-medium font-heading text-lg text-black">
          Lorem ipsum dolor sit.
        </h3>
        <span className="text-gray-500 text-sm mt-3">October, 22 2020</span>
        <span className="text-pink-500 font-medium text-sm">Published</span>
      </div>

      <div className="flex absolute right-3 top-3 group">
        {/* button for detail actions */}
        <div className="flex h-9 w-9 rounded-xl bg-gray-100 justify-center items-center transition-all duration-300 hover:-translate-y-1">
          <Icon
            icon="eva:more-horizontal-outline"
            className="h-5 w-5 text-gray-700"
          />
        </div>

        {/* some menu for the detail */}
        <div className="flex absolute top-0 right-5 bg-white bg-opacity-70 backdrop-blur-2xl border-2 border-gray-100 p-3 rounded-2xl transition-all duration-300 hover:scale-110 hover:-translate-y-2 opacity-0 translate-y-20 group-hover:opacity-100 group-hover:-translate-y-0">
          <ul className="flex flex-col list-none gap-2">
            <li className="h-8 flex w-full font-medium text-sm text-gray-800 items-center px-2 transition-all duration-200 hover:-translate-x-1">
              Edit
            </li>
            <li className="h-8 flex w-full font-medium text-sm text-gray-800 items-center px-2 transition-all duration-200 hover:-translate-x-1">
              Delete
            </li>
          </ul>
        </div>
      </div>
    </div>
  );
}
