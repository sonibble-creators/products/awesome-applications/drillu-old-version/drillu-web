import { Icon } from "@iconify/react";
import { Link } from "@remix-run/react";

/**
 * # MainFooter
 *
 * the footer will use in general purpose
 * @returns JSX.Element
 */
export default function MainFooter(): JSX.Element {
  return (
    <footer className="flex flex-col gap-14 bg-white p-10 bg-opacity-50 backdrop-blur-3xl border-t-4 border-t-white">
      <div className="flex gap-10 mx-auto w-[90%] mt-10">
        <div className="flex flex-col w-4/12">
          <h3 className="font-heading text-4xl font-semibold text-black leading-normal">
            Trusted by thousand company and people
          </h3>
        </div>
        <div className="flex flex-1 mt-5 justify-end">
          <div className="flex h-14">
            <img src="/assets/images/trusted.png" alt="" className="h-full" />
          </div>
        </div>
      </div>

      <div className="flex flex-col w-[80%] mx-auto mt-14">
        <div className="flex w-full gap-5">
          <div className="flex w-1/3 flex-col gap-8">
            <h3 className="font-heading font-semibold text-2xl text-black">
              Informations
            </h3>
            <ul className="flex flex-col list-none gap-3">
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="" className="flex items-center">
                  Home
                </Link>
              </li>
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="/about" className="flex items-center">
                  About
                </Link>
              </li>
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="/contact" className="flex items-center">
                  Contact
                </Link>
              </li>
            </ul>
          </div>
          <div className="flex w-1/3 flex-col gap-8">
            <h3 className="font-heading font-semibold text-2xl text-black">
              Community
            </h3>
            <ul className="flex flex-col list-none gap-3">
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="/partners" className="flex items-center">
                  Partners
                </Link>
              </li>
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="/developers" className="flex items-center">
                  Developers
                </Link>
              </li>
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="/designers" className="flex items-center">
                  Designers
                </Link>
              </li>
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="/certification" className="flex items-center">
                  Certification
                </Link>
              </li>
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="/mentors" className="flex items-center">
                  Mentors
                </Link>
              </li>
            </ul>
          </div>
          <div className="flex w-1/3 flex-col gap-8">
            <h3 className="font-heading font-semibold text-2xl text-black">
              Resources
            </h3>
            <ul className="flex flex-col list-none gap-3">
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="/article" className="flex items-center">
                  Article
                </Link>
              </li>
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="/lesson" className="flex items-center">
                  Lesson
                </Link>
              </li>
              <li className="flex items-center font-medium text-gray-700 text-lg transition-all duration-300 hover:-translate-y-2 hover:text-pink-500">
                <Link to="/course" className="flex items-center">
                  Course
                </Link>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div className="flex border w-full border-gray-100"></div>

      <div className="flex w-full">
        <div className="flex justify-between items-center w-[85%] mx-auto">
          <span className="gap-2 text-lg font-medium flex">
            Copyright © 2020
            <Link
              to=""
              className="text-pink-500 ml-2 transition-all duration-300 hover:-translate-y-1"
            >
              Drillu
            </Link>
          </span>
          <span className="gap-2 text-lg font-medium flex">
            Made with
            <Icon
              icon="ant-design:heart-filled"
              className="text-pink-500 ml-2 transition-all duration-300 hover:-translate-y-1 h-6 w-6"
            />
          </span>
        </div>
      </div>
    </footer>
  );
}
