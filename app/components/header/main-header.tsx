import { Icon } from "@iconify/react";
import { Link } from "@remix-run/react";
import { imageDecoder } from "~/logic/connection/sanity";
import { useAccountStore } from "~/logic/store/account-store";

/**
 * # MainHeader
 *
 * the main header of the application
 * showing some info, like brand, current active account, and more
 * @returns JSX.Element
 */
export default function MainHeader(): JSX.Element {
  const { account, logOut } = useAccountStore((state) => state);

  return (
    <header className="fixed inset-x-0 top-0 z-50 h-20 bg-white bg-opacity-70 backdrop-blur-3xl border-2 border-white flex items-center w-full">
      <div className="flex items-center container mx-auto">
        <div className="flex">
          <Link
            to="/"
            className="flex items-center gap-4 transition-all duration-300 hover:scale-110"
          >
            <img
              src="/assets/images/logo.png"
              alt=""
              className="h-12 w-12 object-fill"
            />
            <h1 className="font-writing text-2xl text-black leading-normal">
              Drillu
            </h1>
          </Link>
        </div>
        <div className="flex ml-40 grow">
          <ul className="list-none flex items-center">
            <li className="flex px-5 py-3 rounded-xl font-medium text-gray-700 text-lg transition-all duration-500 hover:-translate-y-1 hover:text-pink-500">
              <Link to="/article" className="">
                Article
              </Link>
            </li>
            <li className="flex px-5 py-3 rounded-xl font-medium text-gray-700 text-lg transition-all duration-500 hover:-translate-y-1 hover:text-pink-500">
              <Link to="/lesson" className="">
                Lesson
              </Link>
            </li>
            <li className="flex px-5 py-3 rounded-xl font-medium text-gray-700 text-lg transition-all duration-500 hover:-translate-y-1 hover:text-pink-500">
              <Link to="/course" className="">
                Course
              </Link>
            </li>
          </ul>
        </div>

        <div className="flex gap-6">
          {!account && (
            <div className="flex items-center gap-5">
              <Link
                to="/signin"
                className="px-6 py-3 rounded-2xl font-medium text-pink-500 flex justify-center items-center transition-all duration-300 hover:-translate-y-2"
              >
                Sign In
              </Link>
              <Link
                to="/signup"
                className="px-6 py-3 rounded-2xl font-medium bg-pink-500 text-white flex justify-center items-center transition-all duration-300 hover:-translate-y-2"
              >
                Sign Up
              </Link>
            </div>
          )}

          {account && (
            <div className="flex relative group">
              <div className="flex h-12 w-12 rounded-full overflow-hidden border-2 border-white">
                <img
                  src={
                    account.avatar
                      ? imageDecoder(account.avatar).url()
                      : "/assets/images/no-avatar.png"
                  }
                  alt=""
                  className="object-cover h-full w-full"
                />
              </div>

              <div className="flex flex-col gap-1 bg-white rounded-3xl border-2 border-white backdrop-blur-3xl absolute right-10 top-16 w-[280px] p-5 opacity-0 translate-x-400 group-hover:opacity-100 group-hover:translate-x-0 transition-all duration-700">
                <div className="flex flex-col border-b border-b-gray-100 pb-3">
                  <span className="font-medium text-lg">
                    {account.fullName}
                  </span>
                  <span className="text-gray-700">{`@${account.username}`}</span>
                </div>

                <div className="flex">
                  <ul className="list-none flex flex-col gap-3">
                    <li className="flex h-10 rounded-2xl px-3 items-center transition-all duration-300 hover:-translate-x-2">
                      <Link to="/settings" className="flex items-center gap-3">
                        <Icon
                          icon="ep:setting"
                          className="h-6 w-6 text-gray-700"
                        />
                        <span className="font-medium text-gray-700">
                          Settings
                        </span>
                      </Link>
                    </li>
                    <li className="flex h-10 rounded-2xl px-3 items-center transition-all duration-300 hover:-translate-x-2">
                      <Link to="/room" className="flex items-center gap-3">
                        <Icon
                          icon="fluent:breakout-room-20-regular"
                          className="h-6 w-6 text-gray-700"
                        />
                        <span className="font-medium text-gray-700">Room</span>
                      </Link>
                    </li>
                    <li className="flex h-10 rounded-2xl px-3 items-center transition-all duration-300 hover:-translate-x-2">
                      <Link
                        to="/creator-room"
                        className="flex items-center gap-3"
                      >
                        <Icon
                          icon="fluent:breakout-room-20-regular"
                          className="h-6 w-6 text-gray-700"
                        />
                        <span className="font-medium text-gray-700">
                          Creator Room
                        </span>
                      </Link>
                    </li>
                    <li
                      className="flex h-10 rounded-2xl px-3 items-center transition-all duration-300 hover:-translate-x-2"
                      onClick={() => logOut()}
                    >
                      <div className="flex items-center gap-3">
                        <Icon
                          icon="codicon:sign-out"
                          className="h-6 w-6 text-gray-700"
                        />
                        <span className="font-medium text-gray-700">
                          Log Out
                        </span>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          )}
        </div>
      </div>
    </header>
  );
}
