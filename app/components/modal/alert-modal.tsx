import { Icon } from "@iconify/react";
import { AlertModalItemType, useModalStore } from "~/logic/store/modal-store";
import { motion } from "framer-motion";
import { useEffect } from "react";

/**
 * # AlertModal
 *
 * modal to show to user like spring in right side
 * this alert allow to show some information like th process become success, fail or something else
 * also this alert will enable to close by clicking the close modal, and also become automatically close after some time
 * the alert is only show using the vertival view
 * @returns JSX.Element
 */
export default function AlertModal(): JSX.Element {
  const { alerts, autoHideAlert } = useModalStore((state) => state);

  // automatically hide the alert after a few second
  // so we can clear all of the alert
  useEffect(() => {
    autoHideAlert();
  }, [alerts]);

  return (
    <div className="fixed right-10 bottom-14 w-[400px]">
      <motion.div className="flex flex-col gap-3" layout>
        {alerts.map(
          (alert: AlertModalItemType, index: number) =>
            (alert.type == "success" && (
              <SuccessAlertModal data={alert} key={index} />
            )) ||
            (alert.type == "error" && (
              <ErrorAlertModal data={alert} key={index} />
            ))
        )}
      </motion.div>
    </div>
  );
}

/**
 * # AlertModalItemPropsType
 *
 * type for modal alert item
 * some data need to passed like title, message, and some type
 */
type AlertModalItemPropsType = {
  data: AlertModalItemType;
};

/**
 * # SuccessAlertModal
 *
 * modal to showing the success alert
 * when the process become success, this alert will show
 * @returns JSX.Element
 */
export function SuccessAlertModal(props: AlertModalItemPropsType): JSX.Element {
  const { data } = props;
  const { hideAlert } = useModalStore((state) => state);

  return (
    <div className="p-3 rounded-3xl bg-white border-2 border-white bg-opacity-50 backdrop-blur-3xl w-full relative transition-all duration-300 hover:-translate-y-2 hover:scale-105">
      <div className="flex w-full gap-6">
        <div className="flex h-12 w-12 rounded-full bg-green-500 justify-center items-center">
          <Icon icon="akar-icons:check" className="h-6 w-6 text-white" />
        </div>
        <div className="flex flex-col flex-1 gap-1">
          <h5 className="text-green-500 font-medium font-heading">
            {data.title || " Nice, all done"}
          </h5>
          <span className="text-gray-500 text-base leading-normal">
            {data.message || "Hurray the process is success"}
          </span>
        </div>
      </div>

      <div
        className="flex justify-center items-center absolute right-1 top-1 h-8 w-8 rounded-2xl bg-white transition-all duration-200 hover:scale-110"
        onClick={() => hideAlert(data)}
      >
        <Icon icon="gg:close" className="h-4 w-4 text-gray-600" />
      </div>
    </div>
  );
}

/**
 * # ErrorAlertModal
 *
 * modal to showing the alert for error process
 * will showing the error message and also some problem will appears
 * @returns JSX.Element
 */
export function ErrorAlertModal(props: AlertModalItemPropsType): JSX.Element {
  const { data } = props;
  const { hideAlert } = useModalStore((state) => state);

  return (
    <div className="p-3 rounded-3xl bg-white border-2 border-white bg-opacity-50 backdrop-blur-3xl w-full relative transition-all duration-300 hover:-translate-y-2 hover:scale-105">
      <div className="flex w-full gap-6">
        <div className="flex h-12 w-12 rounded-full bg-red-500 justify-center items-center">
          <Icon icon="gg:close" className="h-6 w-6 text-white" />
        </div>
        <div className="flex flex-col flex-1 gap-1">
          <h5 className="text-red-500 font-medium font-heading">
            {data.title || "Opps, something error found."}
          </h5>
          <span className="text-gray-500 text-base leading-normal">
            {data.message ||
              "Something error found when try to run the process, please try again later."}
          </span>
        </div>
      </div>

      <div
        className="flex justify-center items-center absolute right-1 top-1 h-8 w-8 rounded-2xl bg-white transition-all duration-200 hover:scale-110"
        onClick={() => hideAlert(data)}
      >
        <Icon icon="gg:close" className="h-4 w-4 text-gray-600" />
      </div>
    </div>
  );
}
