import { AnimatePresence } from "framer-motion";
import { useModalStore } from "~/logic/store/modal-store";

export default function ConfirmationModal(): JSX.Element {
  const { confirmation, hideConfirmationModal } = useModalStore(
    (state) => state
  );

  return (
    <AnimatePresence>
      {confirmation.isShow && (
        <div className="fixed inset-0 bg-black bg-opacity-20 z-[70] flex justify-center items-center">
          <div className="flex flex-col p-5 rounded-3xl bg-white w-3/12 transition-all duration-300 hover:scale-110">
            <div className="flex flex-col gap-8">
              <h3 className="font-heading text-black text-xl font-semibold leading-tight">
                {confirmation.title || "Are you sure"}
              </h3>
              <span className="text-gray-700 leading-relaxed text-lg">
                {confirmation.message || "this action may cause some changes "}
              </span>
            </div>
            <div className="flex justify-end mt-14 gap-10">
              <button
                className="px-6 py-3 rounded-2xl flex justify-center items-center text-pink-500 font-medium transition-all duration-300 hover:-translate-x-2"
                onClick={() => {
                  hideConfirmationModal();
                  {
                    confirmation.onCancel && confirmation.onCancel();
                  }
                }}
              >
                {confirmation.cancelButtonText || "Cancel"}
              </button>
              <button
                className="px-6 py-3 rounded-2xl flex justify-center items-center text-white bg-pink-500 font-medium transition-all duration-300 hover:-translate-x-2"
                onClick={() => {
                  hideConfirmationModal();
                  {
                    confirmation.onConfirm && confirmation.onConfirm();
                  }
                }}
              >
                {confirmation.confirmButtonText || "Delete"}
              </button>
            </div>
          </div>
        </div>
      )}
    </AnimatePresence>
  );
}
