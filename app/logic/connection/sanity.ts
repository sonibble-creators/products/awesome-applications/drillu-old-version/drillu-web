import sanityClient, { SanityClient } from "@sanity/client";
import ImageUrlBuilder from "@sanity/image-url";

/**
 * # sanityConnection
 *
 * connection to access all of the data through in
 * sanity, so we can doing all service using the client
 */
export const sanityConnection: SanityClient = sanityClient({
  projectId: "2xr156he",
  dataset: "development",
  apiVersion: "2022-01-10",
  token:
    "skAAexZvkKjpoONoNl3kgb5A13B3tnm7U6lY9V4ZpgQU3jXksya7JGcY1HjtTMd5ImK5op7AZi9U1YApTks9A6Td4gyR776LPhzbIiapPVdcrcHWsqsO3SenXBSh7tc51tTSZmdNohExw6snSVk4lIIqXikuPlTXQsR1iWU0wc11iyVZ4InF",
  useCdn: true,
});

// define the url builder
const builder = ImageUrlBuilder(sanityConnection);

/**
 * # imageDecoder
 *
 * decode the image source and get the url
 * of course will enable to get different type of image, size, and colors
 *
 * @param source source file of image
 * @returns ImageUrlBuilder
 */
export const imageDecoder = (source: any) => {
  return builder.image(source);
};
