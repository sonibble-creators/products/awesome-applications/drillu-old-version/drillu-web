import { AccounStatus, AccountRole } from "./account-payload";

/**
 * # SignUpEmailPasswordInput
 *
 * allow to define the data using to signup new user
 * with email password provider
 */
export interface SignUpEmailPasswordInput {
  email: string;
  password: string;
  fullName: string;
  username: string;
  status: AccounStatus;
  roles: AccountRole[];
}

/**
 * # UpdateAccountInput
 *
 * input to update the account data
 * and all data is nullable , so will update only the specify data and
 * defined
 */
export interface UpdateAccountInput {
  email?: string;
  username?: string;
  fullName?: string;
  location?: string;
  skills?: string[];
  bio?: string;
}
