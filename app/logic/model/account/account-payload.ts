/**
 * # AccountPayload
 *
 * the account payload for the document data model
 * access data with specify property defined
 */
export interface AccountPayload {
  _id: string;
  _type: string;
  _createdAt: string;
  _updatedAt: string;
  _rev: string;
  status: AccounStatus;
  email: string;
  cover: any;
  avatar: any;
  username: string;
  password: string;
  fullName: string;
  location: string;
  skills: string[];
  roles: AccountRole[];
  bio: string;
}

/**
 * # AccountStatus
 *
 * the status of the account
 */
export enum AccounStatus {
  ACTIVE = "active",
  DISABLE = "disable",
}

export enum AccountRole {
  ADMIN = "admin",
  USER = "user",
  CREATOR = "creator",
}
