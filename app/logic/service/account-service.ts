import { sanityConnection } from "../connection/sanity";
import { ACCOUNT_DOCUMENT_KEY } from "../data/document-key";
import {
  SignUpEmailPasswordInput,
  UpdateAccountInput,
} from "../model/account/account-input";
import {
  AccounStatus,
  AccountPayload,
  AccountRole,
} from "../model/account/account-payload";
import { v4 } from "uuid";

/**
 * # deleteAccount
 *
 * delete the account
 *
 * @param id id of the account that will be deleted
 * @returns string | undefined
 */
export const deleteAccount: (id: string) => Promise<{
  error: string | undefined;
}> = async (id) => {
  let error: string | undefined;

  // validate the data
  if (!id) error = "Please provide an id";
  if (error) return { error };

  try {
    const result = await sanityConnection.delete(id);
    if (!result) error = "Account not found";

    return { error };
  } catch (err) {
    error = "Opps, something went wrong when deleting your account";
    return { error };
  }
};

/**
 * # changePassword
 *
 * when the user need to change their password, this will be the best
 * way to do that
 * @returns AccountPayload | undefined
 */
export const changePassword: (input: {
  password: string;
  newPassword: string;
  confirmPassword: string;
  id: string;
}) => Promise<{
  data: AccountPayload | undefined;
  error: string | undefined;
}> = async ({ password, newPassword, confirmPassword, id }) => {
  let data: AccountPayload | undefined;
  let error: string | undefined;

  // validate the data including the all input
  if (!password || !newPassword || !confirmPassword) {
    error = "Please fill all the fields";
  }
  if (newPassword !== confirmPassword) {
    error = "Please confirm the correct password";
  }
  if (!id) {
    error = "Cannot perform this action. Please make sure you're login";
  }
  if (error) return { data, error };

  try {
    const current = await sanityConnection.getDocument<AccountPayload>(id);
    if (current?.password != password) {
      error = "The current password is incorrect";
      return { data, error };
    }

    const result = await sanityConnection
      .patch(id)
      .set({ password: newPassword })
      .commit<AccountPayload>();
    data = result as AccountPayload;
    return { data, error };
  } catch (err) {
    error = "Opps, something went wrong when try to update the password";
    return { data, error };
  }
};

/**
 * # changeCover
 *
 * change the user cover, specify some image tobe the cover of acount
 *
 * @returns AccountPayload
 */
export const changeCover: (input: { file: File; id: string }) => Promise<{
  data: AccountPayload | undefined;
  error: string | undefined;
}> = async ({ file, id }) => {
  let data: AccountPayload | undefined;
  let error: string | undefined;

  // validate the data including the all input
  // return some error if the data is not valid
  if (!id || id == "") {
    error = "Opps, please define the id of the account";
  } else if (!file || file.name == "") {
    error = "Opps, please define the file to upload";
  }
  if (error) return { data, error };

  try {
    const assets = await sanityConnection.assets.upload("image", file, {
      filename: `cover/${id}.jpg`,
    });
    if (!assets) {
      error = "Opps, something went wrong when uploading the file";
    }
    const result = await sanityConnection
      .patch(id)
      .set({
        cover: {
          _type: "image",
          asset: { _type: "reference", _ref: assets._id },
        },
      })
      .commit<AccountPayload>();
    data = result as AccountPayload;
    return { data, error };
  } catch (err) {
    error =
      "Opps, unable to update the avatar, something wrong when trying to update. Please try again later.";
    return { data, error };
  }
};

/**
 * # changeAvatar
 *
 * upload the avatar and update the account document
 * @returns AccountPayload
 */
export const changeAvatar: (input: { file: File; id: string }) => Promise<{
  data: AccountPayload | undefined;
  error: string | undefined;
}> = async ({ file, id }) => {
  let data: AccountPayload | undefined;
  let error: string | undefined;

  // validate the data including the all input
  // return some error if the data is not valid
  if (!id || id == "") {
    error = "Opps, please define the id of the account";
  } else if (!file || file.name == "") {
    error = "Opps, please define the file to upload";
  }
  if (error) return { data, error };

  try {
    const assets = await sanityConnection.assets.upload("image", file, {
      filename: `avatar/${id}.jpg`,
    });
    if (!assets) {
      error = "Opps, something went wrong when uploading the file";
    }
    const result = await sanityConnection
      .patch(id)
      .set({
        avatar: {
          _type: "image",
          asset: { _type: "reference", _ref: assets._id },
        },
      })
      .commit<AccountPayload>();
    data = result as AccountPayload;
    return { data, error };
  } catch (err) {
    error =
      "Opps, unable to update the avatar, something wrong when trying to update. Please try again later.";
    return { data, error };
  }
};

/**
 *
 * @param input input to update the account fields
 * @param id id document of the account
 * @returns AccountPayload
 */
export const updateAccount: (
  input: UpdateAccountInput,
  id: string
) => Promise<{
  data: AccountPayload | undefined;
  error: string | undefined;
}> = async (input, id) => {
  let data: AccountPayload | undefined;
  let error: string | undefined;

  // validate all data, and return instead if found an error
  if (!input) {
    error = "Invalid input";
    return { data, error };
  }

  try {
    const result = await sanityConnection
      .patch(id)
      .set({ ...input })
      .commit<AccountPayload>();
    data = result;
    return { data, error };
  } catch (err) {
    error = "Opps, something error when try to update the account data";
    return { data, error };
  }
};

/**
 * # loadAccont
 *
 * load the account data with the specify id of the account
 * and document id
 *
 * @returns AccountPayload
 */
export const loadAccount: (input: { accountId: string }) => Promise<{
  data: AccountPayload | undefined;
  error: string | undefined;
}> = async ({ accountId }) => {
  let data: AccountPayload | undefined;
  let error: string | undefined;

  if (!accountId) {
    error = "Opps, the account id is missing, we cannot fetch the data";
  }
  if (error) {
    return { data, error };
  }

  try {
    const result = await sanityConnection.getDocument<AccountPayload>(
      accountId
    );
    data = result as AccountPayload;
    return { data, error };
  } catch (err) {
    error = "Opps, something error when try to get the account data ";
    return { data, error };
  }
};

/**
 * ## signUpWithEmailAndPassword
 *
 * sign a new user up with email and password
 * will create a new account based on the email, password, and fullname
 *
 * @returns AccountPayload | undefined
 */
export const signUpWithEmailAndPassword: (input: {
  fullName: string;
  email: string;
  password: string;
}) => Promise<{
  data: AccountPayload | undefined;
  error: string | undefined;
}> = async ({ email, password, fullName }) => {
  let data: AccountPayload | undefined;
  let error: string | undefined;

  // validate all data
  // we want to reduce the error visibility
  if (!email || email == "") {
    error = "Email is required, please specify the email";
  } else if (!password || password == "") {
    error = "Password empty, please specify the password";
  } else if (!fullName || fullName == "") {
    error = "Full name empty, please specify the full name";
  }
  if (error) {
    return { data, error };
  }

  try {
    const query = `*[_type == $documentType && email == $email][0]`;
    const params = {
      documentType: ACCOUNT_DOCUMENT_KEY,
      email: email,
    };

    // ensure the account is not exists
    // with the same email
    const sameAccount = await sanityConnection.fetch<AccountPayload>(
      query,
      params
    );
    if (sameAccount) {
      error =
        "Email already exists, Please use another email and ensure become unique ";
      return { data, error };
    }

    const predefinedUsername = email.split("@")[0];
    const input: SignUpEmailPasswordInput = {
      email,
      fullName,
      password,
      username: predefinedUsername,
      status: AccounStatus.ACTIVE,
      roles: [AccountRole.USER],
    };
    const result = await sanityConnection.create({
      ...input,
      ...{ _id: v4(), _type: ACCOUNT_DOCUMENT_KEY },
    });
    data = result as AccountPayload;

    return { data, error };
  } catch (err) {
    error = "Opps, something error when try to sign you up";

    return { data, error };
  }
};

/**
 * # signInWithEmailAndPassword
 *
 * signin the user using the email, password account
 *
 * @returns AccountPayload | undefined
 */
export const signInWithEmailAndPassword: (input: {
  email: string;
  password: string;
}) => Promise<{
  data: AccountPayload | undefined;
  error: string | undefined;
}> = async ({ email, password }) => {
  let data: AccountPayload | undefined;
  let error: string | undefined;

  // validate all data
  // we want to reduce the error visibility
  if (!email || email == "") {
    error = "Email is required, please specify the email";
  } else if (!password || password == "") {
    error = "Password empty, please specify the password";
  }
  if (error) {
    return { data, error };
  }

  try {
    const query = `*[_type == $documentType && email == $email][0]`;
    const params = {
      documentType: ACCOUNT_DOCUMENT_KEY,
      email: email,
    };

    // ensure the account is not exists
    // with the same email
    const result = await sanityConnection.fetch<AccountPayload>(query, params);
    if (!result) {
      error = `User with email : ${email} not found. Please use the correct email or sign up for new user`;
      return { data, error };
    }

    // check the password and make sure it become the same
    if (result.password != password) {
      error = "Wrong password, please type the correct password";
      return {
        data,
        error,
      };
    }

    data = result as AccountPayload;
    return { data, error };
  } catch (err) {
    error = "Opps, something error when try to sign you up";
    return { data, error };
  }
};
