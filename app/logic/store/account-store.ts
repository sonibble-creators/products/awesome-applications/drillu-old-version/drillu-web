import create from "zustand";
import { UpdateAccountInput } from "../model/account/account-input";
import { AccountPayload } from "../model/account/account-payload";
import {
  changeAvatar,
  changeCover,
  changePassword,
  deleteAccount,
  loadAccount,
  signUpWithEmailAndPassword,
  updateAccount,
} from "../service/account-service";
import { useModalStore } from "./modal-store";

/**
 * # AccountStoreType
 *
 * the type of the account store
 * checking all data types of account
 */
type AccountStoreType = {
  account: AccountPayload | undefined;
  deleteAccount: (input: { id: string }) => Promise<{
    error: string | undefined;
  }>;
  changePassword: (input: {
    password: string;
    newPassword: string;
    confirmPassword: string;
    id: string;
  }) => Promise<{
    data: AccountPayload | undefined;
    error: string | undefined;
  }>;
  changeAvatar: (input: { file: File; id: string }) => Promise<void>;
  changeCover: (input: { file: File; id: string }) => Promise<void>;
  loadAccont: (input: { accountId: string }) => Promise<void>;
  logOut: () => void;
  updateAccount: (
    input: UpdateAccountInput,
    id: string
  ) => Promise<{
    data: AccountPayload | undefined;
    error: string | undefined;
  }>;
  signUpWithEmailAndPassword: (input: {
    fullName: string;
    email: string;
    password: string;
  }) => Promise<{
    data: AccountPayload | undefined;
    error: string | undefined;
  }>;
};

/**
 * # useAccountStore
 *
 * the store to manage the account data and state
 * when the account data is changes it will automatically change the data and rebuild the ui
 */
export const useAccountStore = create<AccountStoreType>((set, get) => ({
  account: undefined,
  deleteAccount: async ({ id }) => {
    const { error } = await deleteAccount(id);
    if (!error) {
      set({ account: undefined });
    }
    return { error };
  },
  changePassword: async ({ password, newPassword, confirmPassword, id }) => {
    const { data, error } = await changePassword({
      newPassword,
      confirmPassword,
      id,
      password,
    });
    if (data && !error) {
      set({ account: data });
    }
    return { data, error };
  },
  changeAvatar: async ({ file, id }) => {
    const { showAlert } = useModalStore.getState();
    const { data, error } = await changeAvatar({ file, id });
    if (error) {
      showAlert({
        title: "Failed to change avatar",
        type: "error",
        message: error,
      });
    }
    if (data && !error) {
      set({ account: data });
      showAlert({
        title: "Change Avatar Success",
        message: "Your avatar has been updated",
        type: "success",
      });
    }
  },
  changeCover: async ({ file, id }) => {
    const { showAlert } = useModalStore.getState();
    const { data, error } = await changeCover({ file, id });
    if (error) {
      showAlert({
        title: "Failed to change cover",
        type: "error",
        message: error,
      });
    }
    if (data && !error) {
      set({ account: data });
      showAlert({
        title: "Change cover Success",
        message: "Your cover has been updated",
        type: "success",
      });
    }
  },
  loadAccont: async ({ accountId }) => {
    const { data, error } = await loadAccount({ accountId });
    if (data && !error) {
      set({ account: data });
    }
  },
  logOut: () => {
    const { showConfirmationModal } = useModalStore.getState();
    showConfirmationModal({
      title: "Are you sure want to logout ?",
      message: "You will be logged out of the application",
      confirmButtonText: "Oke",
      onConfirm: async () => {
        set({ account: undefined });
        location.href = "/";
      },
    });
  },
  updateAccount: async (input, id) => {
    const { data, error } = await updateAccount(input, id);
    if (data && !error) {
      set({ account: data });
    }
    return { data, error };
  },
  signUpWithEmailAndPassword: async ({ email, password, fullName }) => {
    const { data, error } = await signUpWithEmailAndPassword({
      email,
      password,
      fullName,
    });
    if (data && !error) {
      set({ account: data });
    }

    return { data, error };
  },
}));
