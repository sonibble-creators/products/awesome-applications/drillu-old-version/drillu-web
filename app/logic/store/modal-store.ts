import create from "zustand";

/**
 * # ModalStoreType
 *
 * type checking for the modal store
 * that contain the state manager about all of modal data
 */
export type ModalStoreType = {
  alerts: AlertModalItemType[];
  confirmation: {
    isShow?: boolean;
    title?: string;
    message?: string;
    confirmButtonText?: string;
    cancelButtonText?: string;
    onConfirm?: () => Promise<void>;
    onCancel?: () => Promise<void>;
  };
  showAlert: (alert: AlertModalItemType) => void;
  hideAlert: (alert: AlertModalItemType) => void;
  autoHideAlert: () => void;
  showConfirmationModal: (input: {
    title?: string;
    message?: string;
    confirmButtonText?: string;
    cancelButtonText?: string;
    onConfirm?: () => Promise<void>;
    onCancel?: () => Promise<void>;
  }) => void;
  hideConfirmationModal: () => void;
};

/**
 * # AlertModalItemType
 *
 * type for the alert type modal store
 * data like the title, message, and more
 */
export type AlertModalItemType = {
  type: "error" | "success";
  title?: string;
  message?: string;
};

/**
 * # useModalStore
 *
 * the store to manage all of the modal
 * to showing the modal, hide, and adding a new alert and something else
 *
 */
export const useModalStore = create<ModalStoreType>((set, get) => ({
  alerts: [],
  confirmation: {},
  showAlert: (alert: AlertModalItemType) => {
    set({ alerts: [...get().alerts, alert] });
  },
  hideAlert: (alert: AlertModalItemType) => {
    const alerts = get().alerts;
    const index = alerts.indexOf(alert);
    if (index > -1) {
      alerts.splice(index, 1);
      set({ alerts });
    }
  },
  autoHideAlert: () => {
    setInterval(() => {
      const alerts = get().alerts;
      alerts.shift();
      set({ alerts });
    }, 5000);
  },
  showConfirmationModal: ({
    message,
    title,
    onCancel,
    onConfirm,
    cancelButtonText,
    confirmButtonText,
  }) => {
    set({
      confirmation: {
        isShow: true,
        message,
        title,
        onCancel,
        onConfirm,
        cancelButtonText,
        confirmButtonText,
      },
    });
  },
  hideConfirmationModal: () => {
    set({ confirmation: {} });
  },
}));
