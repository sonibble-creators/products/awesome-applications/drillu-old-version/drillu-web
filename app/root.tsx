import type {
  LinksFunction,
  LoaderFunction,
  MetaFunction,
} from "@remix-run/node";
import {
  Links,
  LiveReload,
  Meta,
  Outlet,
  Scripts,
  ScrollRestoration,
  useLoaderData,
} from "@remix-run/react";
import { useEffect } from "react";
import appStyles from "~/styles/app.css";
import MainFooter from "./components/footer/main-footer";
import MainHeader from "./components/header/main-header";
import AlertModal from "./components/modal/alert-modal";
import ConfirmationModal from "./components/modal/confirmation-modal";
import { useAccountStore } from "./logic/store/account-store";
import { getSession } from "./sessions";

export const meta: MetaFunction = () => ({
  charset: "utf-8",
  title: "Drillu - Free space for upgrade your skills",
  viewport: "width=device-width,initial-scale=1",
});

export const links: LinksFunction = () => [
  { rel: "icon", type: "image/png", href: "/favicon.ico" },
  { rel: "stylesheet", href: appStyles },
];

export const loader: LoaderFunction = async ({ request }) => {
  const session = await getSession(request.headers.get("Cookie"));
  // check if already account signed to the apps
  if (session.has("accountId")) {
    const accountId = session.get("accountId") as string;
    return {
      accountId,
    };
  }

  return {};
};

/**
 * # App
 *
 * app component to show all of the pages, nested routes and
 * the pretty childs
 *
 *
 * @returns JSX.Element
 */
export default function App() {
  const { loadAccont } = useAccountStore((state) => state);
  const loaderData = useLoaderData();

  useEffect(() => {
    if (loaderData?.accountId) {
      loadAccont({ accountId: loaderData.accountId });
    }
  }, [loaderData]);

  return (
    <html lang="en">
      <head>
        <Meta />
        <Links />
      </head>
      <body className="text-base text-black font-body leading-normal bg-background">
        <MainHeader />
        <div>
          <div className="flex h-[320px] w-[320px] rounded-full bg-yellow-500 fixed left-40 top-40 blur-[320px]"></div>
          <div className="flex h-[320px] w-[320px] rounded-full bg-violet-400 fixed top-[340px] inset-0 m-auto blur-[340px]"></div>
          <div className="flex h-[320px] w-[320px] rounded-full bg-pink-500 fixed right-40 bottom-40 blur-[320px]"></div>
        </div>
        <div className="absolute z-20 inset-0 flex flex-col">
          <Outlet />
          <MainFooter />
        </div>
        <ConfirmationModal />
        <AlertModal />
        <ScrollRestoration />
        <Scripts />
        <LiveReload />
      </body>
    </html>
  );
}
