import { Icon } from "@iconify/react";
import MainHeader from "~/components/header/main-header";

export type ArticleReactionEmojiType = {
  label: string;
  icon: string;
  count: number;
};

const predefinedEmoji: ArticleReactionEmojiType[] = [
  {
    label: "Smile",
    icon: "/assets/images/reactions/beaming-face-with-smiling-eyes.png",
    count: 10,
  },
  {
    label: "Love",
    icon: "/assets/images/reactions/face-blowing-a-kiss.png",
    count: 11,
  },
  {
    label: "Enjoy",
    icon: "/assets/images/reactions/face-with-tears-of-joy.png",
    count: 4,
  },
  {
    label: "Super",
    icon: "/assets/images/reactions/smiling-face-with-heart-eyes.png",
    count: 10,
  },
  {
    label: "Cool",
    icon: "/assets/images/reactions/smiling-face-with-sunglasses.png",
    count: 20,
  },
];

export default function ArticleDetail(): JSX.Element {
  return (
    <>
      <div>
        <div className="flex h-[320px] w-[320px] rounded-full bg-[#FF29EA] fixed left-40 top-40 blur-[320px]"></div>
        <div className="flex h-[320px] w-[320px] rounded-full bg-violet-600 fixed right-40 bottom-40 blur-[320px]"></div>
      </div>

      <MainHeader />

      <main className="flex gap-10 absolute z-20 w-[80%] mx-auto mt-[133px] inset-0">
        <div className="flex flex-col gap-4">
          <div className="flex h-14 w-14 items-center justify-center rounded-2xl bg-white bg-opacity-50 border-2 border-white backdrop-blur-3xl transition-all duration-300 hover:scale-110 hover:-translate-y-2">
            <Icon icon="eva:facebook-fill" className="h-6 w-6 text-black" />
          </div>
          <div className="flex h-14 w-14 items-center justify-center rounded-2xl bg-white bg-opacity-50 border-2 border-white backdrop-blur-3xl transition-all duration-300 hover:scale-110 hover:-translate-y-2">
            <Icon icon="eva:twitter-fill" className="h-6 w-6 text-black" />
          </div>
          <div className="flex h-14 w-14 items-center justify-center rounded-2xl bg-white bg-opacity-50 border-2 border-white backdrop-blur-3xl transition-all duration-300 hover:scale-110 hover:-translate-y-2">
            <Icon icon="carbon:logo-pinterest" className="h-6 w-6 text-black" />
          </div>
        </div>

        <div className="flex flex-col w-7/12 gap-12 mb-40">
          <div className="flex bg-white bg-opacity-50 backdrop-blur-3xl border-2 border-white p-3 rounded-3xl flex-col">
            <div className="flex overflow-hidden rounded-2xl h-[460px] w-full">
              <img
                src="https://images.unsplash.com/photo-1553356084-58ef4a67b2a7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
                alt=""
                className="object-cover h-full w-full"
              />
            </div>

            <div className="flex flex-col mt-10 gap-5 px-5 mb-2">
              <div className="flex gap-3">
                <span className="text-sm font-heading text-primary font-semibold bg-primary bg-opacity-10 rounded-full px-3 py-2">
                  # Tech
                </span>
              </div>
              <h2 className="font-heading text-4xl font-semibold text-black leading-tight">
                The unseen of spending three years at Pixelgrade
              </h2>

              <div className="flex mt-4">
                <span className="text-gray-500">30 mins ago</span>
              </div>
            </div>
          </div>

          <div className="flex bg-white bg-opacity-50 backdrop-blur-3xl border-2 border-white p-6 rounded-3xl flex-col gap-5">
            <p className="text-gray-800 text-lg leading-relaxed font-body">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa cum
              dignissimos delectus quasi! Excepturi fuga aspernatur deleniti
              quisquam adipisci rem alias eum a laudantium in possimus ratione,
              animi officiis eius obcaecati odio asperiores illum accusamus
              reiciendis. Pariatur alias ipsa recusandae?
            </p>
            <p className="text-gray-800 text-lg leading-relaxed font-body">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa cum
              dignissimos delectus quasi! Excepturi fuga aspernatur deleniti
              quisquam adipisci rem alias eum a laudantium in possimus ratione,
              animi officiis eius obcaecati odio asperiores illum accusamus
              reiciendis. Pariatur alias ipsa recusandae?
            </p>
            <p className="text-gray-800 text-lg leading-relaxed font-body">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa cum
              dignissimos delectus quasi! Excepturi fuga aspernatur deleniti
              quisquam adipisci rem alias eum a laudantium in possimus ratione,
              animi officiis eius obcaecati odio asperiores illum accusamus
              reiciendis. Pariatur alias ipsa recusandae?
            </p>
            <div className="flex overflow-hidden rounded-3xl h-[380px] mx-10 mt-6">
              <img
                src="https://images.unsplash.com/photo-1553356084-58ef4a67b2a7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
                alt=""
                className="h-full w-full"
              />
            </div>
            <p className="text-gray-800 text-lg leading-relaxed font-body">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa cum
              dignissimos delectus quasi! Excepturi fuga aspernatur deleniti
              quisquam adipisci rem alias eum a laudantium in possimus ratione,
              animi officiis eius obcaecati odio asperiores illum accusamus
              reiciendis. Pariatur alias ipsa recusandae?
            </p>
            <p className="text-gray-800 text-lg leading-relaxed font-body">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa cum
              dignissimos delectus quasi! Excepturi fuga aspernatur deleniti
              quisquam adipisci rem alias eum a laudantium in possimus ratione,
              animi officiis eius obcaecati odio asperiores illum accusamus
              reiciendis. Pariatur alias ipsa recusandae?
            </p>
            <div className="flex overflow-hidden rounded-3xl h-[380px] mx-10 mt-6">
              <img
                src="https://images.unsplash.com/photo-1553356084-58ef4a67b2a7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
                alt=""
                className="h-full w-full"
              />
            </div>
            <p className="text-gray-800 text-base leading-relaxed font-body">
              Lorem ipsum dolor sit amet consectetur adipisicing elit. Ipsa cum
              dignissimos delectus quasi! Excepturi fuga aspernatur deleniti
              quisquam adipisci rem alias eum a laudantium in possimus ratione,
              animi officiis eius obcaecati odio asperiores illum accusamus
              reiciendis. Pariatur alias ipsa recusandae?
            </p>
          </div>

          <div className="flex flex-col bg-white rounded-3xl bg-opacity-50 backdrop-blur-3xl border-2 border-white p-3">
            <div className="flex p-5 flex-col gap-1">
              <h3 className="font-heading font-semibold text-2xl text-black leading-normal">
                Reactions
              </h3>
              <span className="text-gray-700 leading-normal">
                Some interactions and reaction from the lovers
              </span>
            </div>

            <div className="flex gap-10">
              {predefinedEmoji.map(
                (
                  { count, icon, label }: ArticleReactionEmojiType,
                  index: number
                ) => (
                  <div className="flex flex-col gap-2 items-center relative p-3">
                    {/* teh emoji icons */}
                    <div
                      className="flex h-10 w-10 rounded-xl justify-center items-center"
                      key={index}
                    >
                      <img
                        src={icon}
                        alt=""
                        className="h-full w-full object-cover"
                      />
                    </div>

                    <span className="text-sm text-pink-500 bg-pink-50 px-2 py-1 rounded-full font-medium -mt-4">
                      {count}
                    </span>
                  </div>
                )
              )}
            </div>

            <div className="flex flex-col mt-7 gap-5">
              <textarea
                name=""
                placeholder="Give some reactions"
                className="ring-0 outline-none border border-gray-100 px-5 py-6 rounded-3xl h-40 bg-white"
              ></textarea>
              <div className="flex justify-end">
                <button className="text-white bg-pink-500 rounded-3xl px-5 py-4 text-sm font-medium shadow-xl shadow-pink-200">
                  Comment
                </button>
              </div>
            </div>

            <div className="flex flex-col">
              <div className="flex flex-col gap-4 mt-10">
                {[1, 2, 3, 4, 5].map((index: number) => (
                  <div
                    className="flex flex-col gap-2 p-5 border bg-white border-gray-100 rounded-3xl even:ml-36"
                    key={index}
                  >
                    <span className="font-medium text-sm text-pink-500">
                      Nyoman Sunima
                    </span>
                    <p className="text-gray-700">
                      Lorem ipsum dolor sit amet consectetur, adipisicing elit.
                      Quo, aliquam.
                    </p>
                    <span className="text-gray-500 text-sm">20 mins ago</span>

                    <div className="flex justify-end mt-6">
                      <button className="flex justify-center items-center h-10 w-10 bg-gray-100 rounded-2xl">
                        <Icon
                          icon="ant-design:message-outlined"
                          className="h-6 w-6 text-gray-700"
                        />
                      </button>
                    </div>
                  </div>
                ))}
              </div>

              <div className="flex justify-center mt-10 mb-3">
                <button className="font-medium text-pink-500 bg-gray-50 px-6 py-4 rounded-3xl">
                  Load More
                </button>
              </div>
            </div>
          </div>
        </div>

        <div className="flex flex-col gap-6 w-3/12">
          <div className="flex flex-col bg-white rounded-3xl bg-opacity-50 backdrop-blur-3xl border-2 border-white p-3">
            <div className="flex overflow-hidden rounded-2xl h-[200px] w-full">
              <img
                src="https://images.unsplash.com/photo-1553356084-58ef4a67b2a7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
                alt=""
                className="object-cover h-full w-full"
              />
            </div>

            <div className="flex justify-center items-center h-16 w-16 rounded-2xl border-2 border-white overflow-hidden -mt-8 mx-auto">
              <img
                src="https://images.unsplash.com/photo-1553356084-58ef4a67b2a7?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1887&q=80"
                alt=""
                className="object-cover h-full w-full"
              />
            </div>

            <div className="flex flex-col items-center mt-5 w-full gap-4 mb-5">
              <h4 className="text-black font-semibold font-heading text-xl text-center leading-normal">
                Jordan Layler
              </h4>
              <span className="text-center leading-relxed text-gray-600">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Numquam
                nisi eos corrupti laudantium nobis nulla! Veritatis ex at.
              </span>
            </div>
          </div>

          <div className="flex flex-col bg-white rounded-3xl bg-opacity-50 backdrop-blur-3xl border-2 border-white p-3">
            <div className="flex h-[400px] bg-gradient-to-br from-pink-500 to-cyan-50 rounded-3xl justify-center items-center">
              <span className="font-heading text-4xl text-white font-semibold text-center leading-normal w-10/12">
                Your Banner Here
              </span>
            </div>
          </div>
        </div>
      </main>
    </>
  );
}
