import ArticleItemCard from "~/components/card/article-item-card";
import PopularArticleItemCard from "~/components/card/popular-article-item-card";

/**
 * # Article
 *
 * the article page to show all of the most showing article
 * user enable to see all of the detail article to read
 *
 * @returns JSX.Element
 */
export default function Article(): JSX.Element {
  return (
    <>
      <main className="flex flex-col w-[80%] mx-auto mt-[133px]">
        <section className="flex flex-col gap-5">
          <div className="flex w-full">
            <h3 className="font-heading font-semibold text-3xl">
              Popular this week
            </h3>
          </div>
          <div className="flex w-full overflow-x-auto no-scrollbar py-10 px-5">
            <div className="flex flex-row gap-5">
              {[1, 2, 3, 4, 5, 6].map((index: number) => (
                <PopularArticleItemCard key={index} />
              ))}
            </div>
          </div>
        </section>

        <section className="flex flex-col gap-16 mt-32 pb-32">
          <div className="flex w-full">
            <h3 className="font-heading font-semibold text-3xl">
              Sort for you
            </h3>
          </div>
          <div className="grid grid-cols-4 gap-5">
            {[1, 2, 3, 4, 5, 6, 7, 8].map((index: number) => (
              <ArticleItemCard key={index} />
            ))}
          </div>
        </section>
      </main>
    </>
  );
}
