import { Icon } from "@iconify/react";
import MainHeader from "~/components/header/main-header";
import { useState } from "react";
import CreatorArticleItemCard from "~/components/card/creator-article-item-card";

/**
 * # CreatorRoomSideMenuItemType
 *
 * type for creator room side menu item
 */
type CreatorRoomSideMenuItemType = {
  label: string;
  icon: string;
};

// sideCreatorRoom
// room creator for side menus
const sideCreatorMenus: CreatorRoomSideMenuItemType[] = [
  { label: "Articles", icon: "fluent:document-bullet-list-20-regular" },
];

/**
 * # CreatorRoom
 *
 * creator room page to see all of detail, summary, data , post and much data
 * we will show some summary data and show some status
 *
 * @returns JSX.Element
 */
export default function CreatorRoom(): JSX.Element {
  // define teh acive navigation for the sidebar navigation
  const [activeNav, setActiveNav] = useState<string>("articles");

  // onNavigationClicked
  // handle the navigation click
  const onNavigationClicked: (nav: string) => void = (nav: string) => {
    // set the active navigation
    setActiveNav(nav);
  };

  return (
    <div>
      {/* background with gradient and bluring */}
      <div className="">
        {/* circle one */}
        <div className="flex h-[320px] w-[320px] rounded-full bg-[#FF29EA] fixed left-40 top-40 blur-[320px]"></div>
        <div className="flex h-[320px] w-[320px] rounded-full bg-primary fixed right-40 bottom-40 blur-[320px]"></div>
      </div>

      {/* Header */}
      <MainHeader />

      {/* main content */}
      {/* contain all of content and data */}
      <main className="flex flex-col container mt-[133px] mx-auto absolute inset-0 z-[50]">
        {/* summary section */}
        <section className="flex gap-10">
          {/* profile */}
          <div className="flex w-3/12 p-3 bg-white bg-opacity-50 backdrop-blur-3xl rounded-3xl border-2 border-white flex-col transition-all duration-500 hover:-translate-y-3">
            {/* cover and avatar */}
            <div className="flex h-[200px] rounded-2xl w-full overflow-hidden">
              <img
                src="https://images.unsplash.com/photo-1508898578281-774ac4893c0c?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
                alt=""
                className="object-cover h-full w-full"
              />
            </div>

            <div className="flex mx-auto h-14 w-14 rounded-full border-2 border-white overflow-hidden -mt-8">
              <img
                src="https://images.unsplash.com/photo-1452570053594-1b985d6ea890?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=987&q=80"
                alt=""
                className="h-full w-full object-cover"
              />
            </div>

            {/* the name, username, and summary */}
            <div className="flex flex-col items-center mt-6">
              <h3 className="text-black font-heading text-xl font-medium">
                Ester Named
              </h3>
              <span className="text-gray-600">@esternamed</span>
            </div>

            {/* profile action button */}
            <div className="flex justify-center mt-7">
              <button className="flex py-3 px-6 rounded-full bg-pink-500 text-white font-medium transition-all duration-300 hover:-translate-y-1">
                Profile
              </button>
            </div>
          </div>

          {/* all summary and some info */}
          <div className="flex w-9/12 gap-8 flex-col">
            {/* small summary */}
            <div className="flex w-full gap-6">
              <div className="flex w-1/3 flex-col">
                <div className="flex p-3 rounded-3xl bg-white bg-opacity-50 border-2 border-white backdrop-blur-3xl items-center gap-5 transition-all duration-300 hover:-translate-y-2">
                  <div className="flex justify-center items-center h-16 w-16 rounded-2xl bg-violet-100">
                    <Icon
                      icon="fluent:document-bullet-list-multiple-20-regular"
                      className="h-9 w-9 text-violet-500"
                    />
                  </div>

                  <div className="flex flex-col gap-1">
                    <span className="text-gray-600">Total Articles</span>
                    <span className="font-heading text-2xl text-black font-semibold">
                      100
                    </span>
                  </div>
                </div>
              </div>
              <div className="flex w-1/3 flex-col">
                <div className="flex p-3 rounded-3xl bg-white bg-opacity-50 border-2 border-white backdrop-blur-3xl items-center gap-5 transition-all duration-300 hover:-translate-y-2">
                  <div className="flex justify-center items-center h-16 w-16 rounded-2xl bg-red-100">
                    <Icon
                      icon="akar-icons:heart"
                      className="h-9 w-9 text-red-500"
                    />
                  </div>

                  <div className="flex flex-col gap-1">
                    <span className="text-gray-600">Total Interactions</span>
                    <span className="font-heading text-2xl text-black font-semibold">
                      10K
                    </span>
                  </div>
                </div>
              </div>
              <div className="flex w-1/3 flex-col">
                <div className="flex p-3 rounded-3xl bg-white bg-opacity-50 border-2 border-white backdrop-blur-3xl items-center gap-5 transition-all duration-300 hover:-translate-y-2">
                  <div className="flex justify-center items-center h-16 w-16 rounded-2xl bg-orange-100">
                    <Icon
                      icon="bx:message-square-detail"
                      className="h-9 w-9 text-orange-500"
                    />
                  </div>

                  <div className="flex flex-col gap-1">
                    <span className="text-gray-600">Total Comments</span>
                    <span className="font-heading text-2xl text-black font-semibold">
                      200K
                    </span>
                  </div>
                </div>
              </div>
            </div>

            {/* some performace and other widget */}
            <div className="flex w-full gap-6">
              <div className="flex flex-col w-1/3">
                <div className="flex bg-white bg-opacity-50 rounded-3xl border-2 border-white backdrop-blur-3xl flex-col p-5 transition-all duration-500 hover:-translate-y-2">
                  <div className="flex flex-col gap-2">
                    <h4 className="text-xl font-semibold font-heading text-black">
                      Perfomance
                    </h4>
                    <span className="text-gray-600 leading-tight">
                      Forformace for your action to change the world
                    </span>
                  </div>

                  <div className="flex justify-center items-center"></div>
                  <div className="flex mt-14">
                    <h3 className="text-green-400 font-heading text-4xl font-medium">
                      GOOD
                    </h3>
                  </div>
                </div>
              </div>

              <div className="flex flex-col w-1/3">
                <div className="flex flex-col bg-pink-500 p-5 rounded-3xl items-end relative transition-all duration-500 hover:-translate-y-2">
                  <div className="flex flex-col w-1/2">
                    <h2 className="text-white font-heading text-2xl font-semibold text-right">
                      Enhance your creativity
                    </h2>
                  </div>
                  <div className="flex mt-10">
                    <button className="flex px-5 py-3 rounded-2xl bg-white text-pink-500 font-medium transition-all duration-300 hover:-translate-y-2">
                      Read more
                    </button>
                  </div>

                  <img
                    src={""}
                    alt=""
                    className="absolute left-0 bottom-0 w-8/12"
                  />
                </div>
              </div>
            </div>
          </div>
        </section>

        {/* posta and other section */}
        <section className="flex gap-10 w-full mt-32">
          {/* navigation */}

          <div className="flex flex-col gap-6 w-[300px]">
            <div className="flex p-3 rounded-3xl bg-white bg-opacity-50 backdrop-blur-3xl border-2 border-white">
              <ul className="list-none flex flex-col gap-4 w-full">
                {sideCreatorMenus.map(
                  (
                    { icon, label }: CreatorRoomSideMenuItemType,
                    index: number
                  ) => (
                    <li
                      className={`flex items-center gap-3 text-black h-12 rounded-xl w-full px-4 transition-all duration-300 hover:-translate-x-2 ${
                        label.toLowerCase() == activeNav
                          ? "bg-pink-500 text-white"
                          : ""
                      }`}
                      key={index}
                      onClick={() => onNavigationClicked(label.toLowerCase())}
                    >
                      <Icon icon={icon} className="h-5 w-5" />
                      <span className="font-medium">{label}</span>
                    </li>
                  )
                )}
              </ul>
            </div>
          </div>

          {/* the content should be here */}
          <div className="flex flex-col w-10/12">
            {activeNav == "articles" && <CreatorRoomArticle />}
          </div>
        </section>
      </main>

      {/* footer */}
    </div>
  );
}

/**
 * # CreatorRoomArticle
 *
 * the article components for the creator room
 *
 * @returns JSX.Element
 */
export function CreatorRoomArticle(): JSX.Element {
  return (
    <div className="bg-white p-5 rounded-3xl bg-opacity-50 backdrop-blur-3xl border-2 border-white">
      <div className="flex">
        {/* title */}
        <h3 className="font-heading text-xl font-semibold text-black">
          Articles
        </h3>
      </div>

      <div className="flex flex-row items-center gap-6 mt-6">
        {/* searchbar */}
        <div className="flex h-12 rounded-2xl border border-gray-200 w-4/12 items-center px-4">
          <input
            type="text"
            className="ring-0 outline-none border-0 bg-transparent w-full h-full"
            placeholder="Search your favourite article"
          />
          <Icon icon="akar-icons:search" className="w-4 h4 text-gray-400" />
        </div>

        {/* sort by */}
        <div className="flex w-2/12 rounded-2xl border border-gray-200 h-12 px-4">
          <select
            name=""
            id=""
            className="w-full bg-transparent h-full ring-0 outline-none border-none"
          >
            <option>Order By</option>
            <option>Name</option>
            <option>Created</option>
            <option>Status</option>
          </select>
        </div>
      </div>

      {/* all content  */}
      <div className="grid grid-cols-2 gap-3 mt-12">
        {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12].map((index: number) => (
          <CreatorArticleItemCard key={index} />
        ))}
      </div>

      {/* navigation */}
      <div className="flex gap-6 items-center justify-end mt-10">
        <div className="flex h-11 w-11 rounded-xl bg-white justify-center items-center">
          <Icon icon="akar-icons:chevron-left" className="h-5 w-5 text-black" />
        </div>
        <div className="flex h-11 w-11 rounded-xl bg-white justify-center items-center">
          <Icon
            icon="akar-icons:chevron-right"
            className="h-5 w-5 text-black"
          />
        </div>
      </div>
    </div>
  );
}
