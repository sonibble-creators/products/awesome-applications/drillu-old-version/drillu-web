import { Icon } from "@iconify/react";
import { NavLink, Outlet } from "@remix-run/react";

// type of setting menu item
type SettingMenuItemType = {
  label: string;
  icon: string;
  link: string;
};

// ! default setting menu
// define the setting menu on the left sidebar
// and will become the navigation for the nested routes
// TODO: Before jump and create the other menu, please add the nested page first
const settingMenuItems: SettingMenuItemType[] = [
  { label: "Profile", icon: "iconoir:profile-circled", link: "" },
  {
    label: "Security",
    icon: "carbon:security",
    link: "security",
  },
  {
    label: "Account",
    icon: "icon-park-outline:category-management",
    link: "account",
  },
];

/**
 * # Settings
 *
 * the settings component to handle all of the setting logic
 * continue with their child element for example like account, password, profile and so on.
 *
 * @returns JSX.Element
 */
export default function Settings(): JSX.Element {
  return (
    <div>
      <div className="">
        {/* circle one */}
        <div className="flex h-[320px] w-[320px] rounded-full bg-[#FF29EA] fixed left-40 top-40 blur-[320px]"></div>
        <div className="flex h-[320px] w-[320px] rounded-full bg-primary fixed right-40 bottom-40 blur-[320px]"></div>
      </div>

      <main className="flex flex-col w-[80%] mt-[133px] mx-auto absolute inset-0 gap-10">
        <div className="flex">
          <div className="flex">
            <h2 className="font-heading font-semibold text-3xl text-black">
              Settings
            </h2>
          </div>
        </div>
        <div className="flex w-full gap-10">
          <div className="flex w-[260px] flex-col">
            <div className="flex p-3 rounded-3xl bg-white bg-opacity-50 backdrop-blur-3xl border-2 border-white">
              <ul className="list-none flex flex-col w-full gap-3">
                {/* list all of the side menu for setting */}
                {/* when user clicked it will bring into the nested layout */}
                {/* using the navlink is had an advanced feature to handle all of this */}
                {settingMenuItems.map(
                  (
                    { icon, label, link }: SettingMenuItemType,
                    index: number
                  ) => (
                    <li
                      className="flex items-center h-12 rounded-2xl overflow-hidden text-gray-800 w-full transition-all duration-300 hover:-translate-x-2"
                      key={index}
                    >
                      <NavLink
                        to={link}
                        className={({ isActive }) =>
                          `flex items-center gap-4 h-full w-full px-4 rounded-2xl transition-all duration-300 ${
                            isActive && "text-pink-500 bg-pink-100 "
                          }`
                        }
                        end
                      >
                        <Icon icon={icon} className="h-6 w-6" />
                        <span className="font-medium">{label}</span>
                      </NavLink>
                    </li>
                  )
                )}
              </ul>
            </div>
          </div>
          <div className="flex flex-col w-7/12">
            <Outlet />
          </div>
        </div>
      </main>
    </div>
  );
}
