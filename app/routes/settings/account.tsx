import { useAccountStore } from "~/logic/store/account-store";
import { useModalStore } from "~/logic/store/modal-store";

/**
 * # AccountSettings
 *
 * setting for the account
 * @returns JSX.Element
 */
export default function AccountSettings(): JSX.Element {
  const { account, deleteAccount } = useAccountStore((state) => state);
  const { showAlert, showConfirmationModal } = useModalStore((state) => state);

  const onDeleteClicked: (input: { id: string }) => Promise<void> = async ({
    id,
  }) => {
    showConfirmationModal({
      title: "Are you sure to delete the account?",
      message:
        "you will kickout from the application, all creative idea will be deleted",
      onConfirm: async () => {
        const { error } = await deleteAccount({ id });
        if (error) {
          showAlert({
            title: "Failed to delete account",
            message: error,
            type: "error",
          });
        } else {
          showAlert({
            title: "Account deleted",
            message: "you have been kicked out of the application",
            type: "success",
          });

          location.href = "/";
        }
      },
    });
  };

  return (
    <div className="flex flex-col gap-8">
      <div className="flex bg-white bg-opacity-50 backdrop-blur-3xl rounded-3xl border-2 border-white p-5 flex-col transition-all duration-300 hover:-translate-y-2">
        <div className="flex flex-col gap-4">
          <h3 className="font-medium text-2xl font-heading text-black">
            Delete Account
          </h3>
          <span className="text-gray-600 leading-normal">
            Delete the account will delete all of the current data, you will
            never been again enable to use the rich feature of application
          </span>
        </div>

        <div className="flex mt-14">
          <button
            className="h-12 flex justify-center items-center font-medium text-white bg-red-500 rounded-2xl w-full transition-all duration-200 hover:shadow-lg hover:shadow-red-200 disabled:bg-red-200 disabled:text-red-500"
            onClick={() => onDeleteClicked({ id: account?._id! })}
          >
            Delete Account
          </button>
        </div>
      </div>
    </div>
  );
}
