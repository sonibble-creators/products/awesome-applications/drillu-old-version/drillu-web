import { Icon } from "@iconify/react";
import { ActionFunction } from "@remix-run/node";
import { Form, useActionData, useTransition } from "@remix-run/react";
import { useEffect } from "react";
import { imageDecoder } from "~/logic/connection/sanity";
import { UpdateAccountInput } from "~/logic/model/account/account-input";
import { useAccountStore } from "~/logic/store/account-store";
import { useModalStore } from "~/logic/store/modal-store";

export const action: ActionFunction = async ({ request }) => {
  const { updateAccount } = useAccountStore.getState();
  const formData = await request.formData();
  const action = formData.get("action");
  if (action == "save") {
    const accountId = formData.get("accountId") as string;
    const skills = formData.get("skills") as string;
    const splitSkills = skills.split(",");
    const input: UpdateAccountInput = {
      bio: formData.get("bio") as string,
      email: formData.get("email") as string,
      fullName: formData.get("fullName") as string,
      location: formData.get("location") as string,
      username: formData.get("username") as string,
      skills: splitSkills,
    };
    const { data, error } = await updateAccount(input, accountId);
    return { data, error };
  }

  return {};
};

/**
 * # ProfileSetting
 *
 * the setting component to manage all of the profile setting logic
 * will contain some information like avatar, name, email, username, cover and, etc.
 *
 * @returns JSX.Element
 */
export default function ProfileSetting(): JSX.Element {
  const { account, changeAvatar, changeCover } = useAccountStore(
    (state) => state
  );
  const { showAlert } = useModalStore.getState();
  const transition = useTransition();
  const actionData = useActionData();

  // when the save account
  // will trigger and showing the alert of progress
  useEffect(() => {
    if (actionData?.error) {
      showAlert({
        title: "Opps, Update account failed",
        message: actionData.error,
        type: "error",
      });
    }
    if (actionData?.data) {
      showAlert({
        title: "Update account success",
        message: "Your account has been updated",
        type: "success",
      });
    }
  }, [actionData]);

  return (
    <div className="flex flex-col gap-8">
      <Form method="post" className="flex flex-col gap-8">
        <div className="flex bg-white bg-opacity-50 backdrop-blur-3xl rounded-3xl border-2 border-white p-3 flex-col  transition-all duration-300 hover:-translate-y-2">
          <div className="flex rounded-2xl overflow-hidden h-[274px] w-full relative">
            <img
              src={
                account?.cover
                  ? imageDecoder(account?.cover).url()
                  : "https://images.unsplash.com/photo-1550684848-fac1c5b4e853?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1740&q=80"
              }
              alt=""
              className="h-full w-full object-cover"
            />

            <label
              htmlFor="cover"
              className="flex h-10 w-10 bg-white rounded-2xl justify-center items-center absolute right-2 top-2 transition-all duration-300 hover:scale-110"
            >
              <Icon
                icon="akar-icons:camera"
                className="h-5 w-5 text-gray-800"
              />
              <input
                type="file"
                className="sr-only"
                id="cover"
                onChange={(e) =>
                  changeCover({ file: e.target.files![0]!, id: account?._id! })
                }
              />
            </label>
          </div>

          <div className="flex mx-auto -mt-7 relative">
            <div className="flex h-[80px] w-[80px] rounded-full border-2 border-white overflow-hidden relative transition-all duration-300 hover:-translate-y-2">
              <img
                alt=""
                src={
                  account?.avatar
                    ? imageDecoder(account?.avatar).url()
                    : "/assets/images/no-avatar.png"
                }
                className="h-full w-full object-cover"
              />
            </div>
            <label
              htmlFor="avatar"
              className="flex h-9 w-9 bg-white rounded-2xl justify-center items-center absolute -right-2 -bottom-1 transition-all duration-300 hover:scale-110"
            >
              <Icon
                icon="akar-icons:camera"
                className="h-5 w-5 text-gray-800"
              />
              <input
                type="file"
                className="sr-only"
                id="avatar"
                onChange={(e) =>
                  changeAvatar({ file: e.target.files![0], id: account?._id! })
                }
              />
            </label>
          </div>
        </div>

        <div className="flex bg-white bg-opacity-50 backdrop-blur-3xl rounded-3xl border-2 border-white p-5 flex-col transition-all duration-300 hover:-translate-y-2">
          <div className="flex">
            <h3 className="font-medium text-2xl font-heading text-black">
              User
            </h3>
          </div>

          <div className="flex flex-col mt-10 gap-5">
            <input type="hidden" name="accountId" defaultValue={account?._id} />
            <div className="flex group flex-col gap-2">
              <label className="ml-2 text-gray-500 font-medium leading-normal">
                Full Name
              </label>
              <input
                type="text"
                placeholder="Your full name"
                name="fullName"
                defaultValue={account?.fullName}
                className="flex w-full bg-transparent rounded-2xl border border-gray-200 h-12 px-4 outline-none ring-0 focus:border-2 focus:border-pink-300 transition-all duration-500 hover:-translate-x-1"
              />
            </div>
            <div className="flex group flex-col gap-2">
              <label className="ml-2 text-gray-500 font-medium leading-normal">
                Username
              </label>
              <input
                type="text"
                placeholder="Your username"
                name="username"
                defaultValue={account?.username}
                className="flex w-full bg-transparent rounded-2xl border border-gray-200 h-12 px-4 outline-none ring-0 focus:border-2 focus:border-pink-300 transition-all duration-500 hover:-translate-x-1"
              />
            </div>
            <div className="flex group flex-col gap-2">
              <label className="ml-2 text-gray-500 font-medium leading-normal">
                Email address
              </label>
              <input
                type="email"
                placeholder="Your email address"
                name="email"
                defaultValue={account?.email}
                className="flex w-full bg-transparent rounded-2xl border border-gray-200 h-12 px-4 outline-none ring-0 focus:border-2 focus:border-pink-300 transition-all duration-500 hover:-translate-x-1"
              />
            </div>
          </div>
        </div>

        <div className="flex bg-white bg-opacity-50 backdrop-blur-3xl rounded-3xl border-2 border-white p-5 flex-col transition-all duration-300 hover:-translate-y-2">
          <div className="flex">
            <h3 className="font-medium text-2xl font-heading text-black">
              Basic
            </h3>
          </div>

          <div className="flex flex-col mt-10 gap-5">
            <div className="flex group flex-col gap-2">
              <label className="ml-2 text-gray-500 font-medium leading-normal">
                Location
              </label>
              <input
                type="text"
                placeholder="Your location"
                name="location"
                defaultValue={account?.location}
                className="flex w-full bg-transparent rounded-2xl border border-gray-200 h-12 px-4 outline-none ring-0 focus:border-2 focus:border-pink-300 transition-all duration-500 hover:-translate-x-1"
              />
            </div>
            <div className="flex group flex-col gap-2">
              <label className="ml-2 text-gray-500 font-medium leading-normal">
                Skills
              </label>
              <input
                type="text"
                placeholder="Your skills seperate by coma ','"
                name="skills"
                defaultValue={account?.skills?.join(",")}
                className="flex w-full bg-transparent rounded-2xl border border-gray-200 h-12 px-4 outline-none ring-0 focus:border-2 focus:border-pink-300 transition-all duration-500 hover:-translate-x-1"
              />
            </div>

            <div className="flex group flex-col gap-2">
              <label className="ml-2 text-gray-500 font-medium leading-normal">
                Bio
              </label>
              <textarea
                placeholder="Describe your self"
                name="bio"
                defaultValue={account?.bio}
                className="flex w-full bg-transparent  rounded-2xl border border-gray-200 min-h-[200px] py-5 px-4 outline-none ring-0 focus:border-2 focus:border-pink-300 transition-all duration-500 hover:-translate-x-1"
              ></textarea>
            </div>
          </div>
        </div>

        <div className="flex bg-white bg-opacity-50 backdrop-blur-3xl rounded-3xl border-2 border-white p-5 flex-col transition-all duration-300 hover:-translate-y-2 mb-20">
          <div className="flex w-full">
            <button
              type="submit"
              name="action"
              value="save"
              disabled={transition.state == "submitting"}
              className="h-12 flex justify-center items-center font-medium text-white bg-pink-500 rounded-2xl w-full transition-all duration-200 hover:shadow-lg hover:shadow-pink-200 disabled:bg-pink-200 disabled:text-pink-500"
            >
              {transition.state == "submitting" ? (
                <span className="flex gap-2 items-center">
                  <Icon icon="eos-icons:loading" className="" />
                  <span>Saving</span>
                </span>
              ) : (
                <span>Save</span>
              )}
            </button>
          </div>
        </div>
      </Form>
    </div>
  );
}
