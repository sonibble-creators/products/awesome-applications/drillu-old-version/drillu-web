import { Icon } from "@iconify/react";
import { ActionFunction } from "@remix-run/node";
import { Form, useActionData, useTransition } from "@remix-run/react";
import { useEffect } from "react";
import AlertModal from "~/components/modal/alert-modal";
import { useAccountStore } from "~/logic/store/account-store";
import { useModalStore } from "~/logic/store/modal-store";

export const action: ActionFunction = async ({ request }) => {
  const { changePassword } = useAccountStore.getState();
  const formData = await request.formData();
  const action = formData.get("action");
  if (action == "save") {
    const accountId = formData.get("accountId") as string;
    const currentPassword = formData.get("current-password") as string;
    const newPassword = formData.get("new-password") as string;
    const confirmPassword = formData.get("confirm-password") as string;
    const { data, error } = await changePassword({
      confirmPassword,
      id: accountId,
      newPassword,
      password: currentPassword,
    });
    return { data, error };
  }
  return {};
};

/**
 * # SecuritySettings
 *
 * account setting page for the user maybe to change the password,
 * delete account, and etc.
 * @returns JSX.Element
 */
export default function SecuritySettings(): JSX.Element {
  const { showAlert } = useModalStore((state) => state);
  const { account } = useAccountStore((state) => state);
  const transition = useTransition();
  const actionData = useActionData();

  // when the user save the data and start execute
  // this method will handle the data and error comming,
  // so we can see the detail problem happened
  useEffect(() => {
    if (actionData?.error) {
      showAlert({ type: "error", message: actionData?.error });
    } else if (actionData?.data) {
      showAlert({ type: "success", message: "Nice, your data are updated." });
    }
  }, [actionData]);

  return (
    <div className="flex flex-col gap-8">
      <Form method="post" className="flex flex-col gap-8">
        <div className="flex bg-white bg-opacity-50 backdrop-blur-3xl rounded-3xl border-2 border-white p-5 flex-col transition-all duration-300 hover:-translate-y-2">
          <div className="flex">
            <h3 className="font-medium text-2xl font-heading text-black">
              Change Password
            </h3>
          </div>

          <div className="flex flex-col mt-10 gap-5">
            <input name="accountId" defaultValue={account?._id} hidden />
            <div className="flex group flex-col gap-2">
              <label className="ml-2 text-gray-500 font-medium leading-normal">
                Current Password
              </label>
              <input
                type="password"
                placeholder="Your current password"
                name="current-password"
                className="flex w-full bg-transparent rounded-2xl border border-gray-200 h-12 px-4 outline-none ring-0 focus:border-2 focus:border-pink-300 transition-all duration-500 hover:-translate-x-1"
              />
            </div>
            <div className="flex group flex-col gap-2">
              <label className="ml-2 text-gray-500 font-medium leading-normal">
                New password
              </label>
              <input
                type="password"
                placeholder="Your new password"
                name="new-password"
                className="flex w-full bg-transparent rounded-2xl border border-gray-200 h-12 px-4 outline-none ring-0 focus:border-2 focus:border-pink-300 transition-all duration-500 hover:-translate-x-1"
              />
            </div>
            <div className="flex group flex-col gap-2">
              <label className="ml-2 text-gray-500 font-medium leading-normal">
                Confirm new password
              </label>
              <input
                type="password"
                placeholder="Confirm your new password"
                name="confirm-password"
                className="flex w-full bg-transparent rounded-2xl border border-gray-200 h-12 px-4 outline-none ring-0 focus:border-2 focus:border-pink-300 transition-all duration-500 hover:-translate-x-1"
              />
            </div>
          </div>
        </div>

        <div className="flex bg-white bg-opacity-50 backdrop-blur-3xl rounded-3xl border-2 border-white p-5 flex-col transition-all duration-300 hover:-translate-y-2 mb-20">
          <div className="flex w-full">
            <button
              type="submit"
              name="action"
              value="save"
              disabled={transition.state == "submitting"}
              className="h-12 flex justify-center items-center font-medium text-white bg-pink-500 rounded-2xl w-full transition-all duration-200 hover:shadow-lg hover:shadow-pink-200 disabled:bg-pink-200 disabled:text-pink-500"
            >
              {transition.state == "submitting" ? (
                <span className="flex gap-2 items-center">
                  <Icon icon="eos-icons:loading" className="" />
                  <span>Saving</span>
                </span>
              ) : (
                <span>Save</span>
              )}
            </button>
          </div>
        </div>
      </Form>
    </div>
  );
}
