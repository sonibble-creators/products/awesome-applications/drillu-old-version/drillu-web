import { ActionFunction, json, MetaFunction, redirect } from "@remix-run/node";
import { Form, Link, useActionData, useTransition } from "@remix-run/react";
import { useEffect, useState } from "react";
import { getSession, commitSession } from "~/sessions";
import { Icon } from "@iconify/react";
import { LoaderFunction } from "@remix-run/node";
import { useAccountStore } from "~/logic/store/account-store";

export const meta: MetaFunction = () => ({
  title: "Signup - Become the member of tousand of creative people",
  description: "Sign up now, and become the part of creative world",
});

export const loader: LoaderFunction = async ({ request }) => {
  const session = await getSession(request.headers.get("Cookie"));

  // check if already account signed to the apps
  if (session.has("accountId")) {
    return redirect("/");
  }

  return json({
    headers: {
      "Set-Cookie": await commitSession(session),
    },
  });
};

export const action: ActionFunction = async ({ request }) => {
  const session = await getSession(request.headers.get("Cookie"));
  const { signUpWithEmailAndPassword } = useAccountStore.getState();
  const formData = await request.formData();
  const action = formData.get("action") as string;

  if (action == "signup") {
    const email = formData.get("email") as string;
    const password = formData.get("password") as string;
    const fullName = formData.get("fullName") as string;

    const { data, error } = await signUpWithEmailAndPassword({
      email,
      fullName,
      password,
    });

    // check if data we send already exists
    // set the cookie sessions and back to home
    if (data && !error) {
      session.set("accountId", data._id);
      return redirect("/", {
        headers: {
          "Set-Cookie": await commitSession(session),
        },
      });
    }

    return { data, error };
  }

  return {};
};

/**
 * # SignUp
 *
 * When user want to become a member, they will signup using the special technique
 * and inut some of element like name, email, password
 * @returns JSX.Element
 */
export default function SignUp(): JSX.Element {
  const transition = useTransition();
  const actionData = useActionData();
  const [signErrorMessagge, setSignErrorMessage] = useState<
    string | undefined
  >();

  useEffect(() => {
    if (actionData?.error) {
      setSignErrorMessage(actionData.error);
    } else {
      setSignErrorMessage(undefined);
    }
  }, [actionData]);

  return (
    <>
      <div>
        <div className="flex h-[320px] w-[320px] rounded-full bg-[#FF29EA] fixed left-40 top-40 blur-[320px]"></div>
        <div className="flex h-[320px] w-[320px] rounded-full bg-violet-600 fixed right-40 bottom-40 blur-[320px]"></div>
      </div>

      <main className="flex flex-col absolute z-30">
        <section className="flex gap-20">
          <div className="flex w-1/2 h-screen">
            <img
              src="/assets/images/signup.png"
              alt=""
              className="object-cover h-full w-full"
            />
          </div>
          <div className="flex flex-col justify-center w-5/12">
            <div className="flex flex-col gap-5">
              <span className="font-heading text-lg font-semibold text-pink-600">
                Sign In .
              </span>
              <h2 className="font-heading text-5xl font-bold text-black leading-tight">
                Become the member of tousand creative people
              </h2>
            </div>

            <div className="flex flex-col w-7/12 mt-20">
              <Form method="post" className="flex flex-col gap-5">
                <div className="flex flex-col gap-2">
                  <input
                    type="text"
                    name="fullName"
                    className="h-12 rounded-2xl border border-gray-200 outline-none ring-0 flex items-center px-4 focus:border-2 focus:border-pink-500 transition-all duration-300 hover:-translate-x-2 bg-white bg-opacity-50 backdrop-blur-3xl"
                    placeholder="Your full name"
                  />
                </div>
                <div className="flex flex-col gap-2">
                  <input
                    type="email"
                    name="email"
                    className="h-12 rounded-2xl border border-gray-200 outline-none ring-0 flex items-center px-4 focus:border-2 focus:border-pink-500 transition-all duration-300 hover:-translate-x-2 bg-white bg-opacity-50 backdrop-blur-3xl"
                    placeholder="Your email address"
                  />
                </div>
                <div className="flex flex-col gap-2">
                  <input
                    type="password"
                    name="password"
                    className="h-12 rounded-2xl border border-gray-200 outline-none ring-0 flex items-center px-4 focus:border-2 focus:border-pink-500 transition-all duration-300 hover:-translate-x-2 bg-white bg-opacity-50 backdrop-blur-3xl"
                    placeholder="Your password"
                  />
                </div>

                <div className="flex gap-2 justify-end mt-4">
                  <span className="text-gray-700">
                    Already have an account ?
                  </span>
                  <Link to="/signin" className="font-medium text-pink-500">
                    Sign In
                  </Link>
                </div>

                {signErrorMessagge && transition.state != "submitting" && (
                  <div className="flex mt-3">
                    <div className="flex w-full p-3 rounded-2xl bg-red-200 text-red-500">
                      {signErrorMessagge}
                    </div>
                  </div>
                )}

                <div className="flex mt-8">
                  <button
                    type="submit"
                    name="action"
                    value="signup"
                    disabled={transition.state == "submitting"}
                    className="flex justify-center items-center font-medium bg-pink-500 rounded-2xl h-12 w-full text-white transition-all duration-500 hover:scale-110 disabled:bg-pink-200 disabled:text-pink-500"
                  >
                    {transition.state == "submitting" ? (
                      <span className="flex gap-2 items-center">
                        <Icon icon="eos-icons:loading" className="w-4 h-4" />
                        <span>Signing Up</span>
                      </span>
                    ) : (
                      <span>Sign Up</span>
                    )}
                  </button>
                </div>
              </Form>
            </div>
          </div>
        </section>
      </main>
    </>
  );
}
