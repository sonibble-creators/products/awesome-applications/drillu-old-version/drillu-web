import { createCookieSessionStorage } from "@remix-run/node";

const { getSession, commitSession, destroySession } =
  createCookieSessionStorage({
    cookie: {
      name: "__session",

      // all of these are optional
      // domain: "remix.run",
      expires: new Date(Date.now() + 6000000),
      httpOnly: true,
      maxAge: 6000000,
      path: "/",
      sameSite: "lax",
      secrets: ["64vfvfet45bioplM*^^*45"],
      secure: true,
    },
  });

export { getSession, commitSession, destroySession };
